# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.10.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + tags=[]
# %matplotlib inline

# + tags=[]
import salvus.namespace as sn
from salvus.mesh import simple_mesh
from salvus.mesh.unstructured_mesh import UnstructuredMesh
from salvus.flow import api
import numpy as np
import os
from matplotlib import pyplot as plt


import sys
sys.path.append('./mass/')
from elemental_matrices import get_jacobian
from basis_polynomials import lagrange_basis_derivative_matrix
# # +
TENSOR_ORDER = int(os.environ.get("TENSOR_ORDER", 2))
NUM_RANKS = int(os.environ.get("NUM_RANKS", 8))
INITIAL_VALUES = True if int(os.environ.get("INITIAL_VALUES", 0)) == 1 else False
PRECONDITIONER = False if int(os.environ.get("PRECONDITIONER", 0)) == 1 else True
MASS_SCALING = False if int(os.environ.get("PRECONDITIONER", 0)) == 1 else True

MASS_SCALING=False

mflag = "w/" if MASS_SCALING else "w/o"
pflag = "w/" if PRECONDITIONER else "w/o"
iflag = "w/" if INITIAL_VALUES else "w/o"

VERBOSITY = int(os.environ.get("VERBOSITY", 0))
TOTAL_VOLUME = 1
print(
    f"Running 2D Cartesian Poisson problem on {NUM_RANKS} rank(s):\n"
    f"- order {TENSOR_ORDER},\n"
    f"- {mflag} mass scaling,\n"
    f"- {pflag} preconditioner,\n"
    f"- {iflag} initial values."
)
G = 6.67408*10**-11
M1 = 1/(4*np.pi*G)
M1 = 1


# +
def misfit(d_obs, U, A, P):
    return A.dot((d_obs - U).ravel()).T.dot(P).dot(A).dot((d_obs - U).ravel())


def gradient(mesh, field='rhs', order = 4, ndim=2):
    from basis_polynomials import lagrange_basis_derivative_matrix,  lagrange_basis_polynomials
    from quadrature_points_weights import gauss_lobatto_legendre_quadruature_points_weights, gauss_quadruature_points_weights
    assert(field in mesh.element_nodal_fields.keys())
    
    points = gauss_lobatto_legendre_quadruature_points_weights(order+1)[0]
    n = len(points)
    J = get_jacobian(mesh)
    l = np.identity(n)
    dl = lagrange_basis_derivative_matrix(points)

    result = np.zeros((mesh.nelem, n**2, 2))
    
    for nelem in range(mesh.nelem):
        derivative_u_local = np.zeros((n**2, ndim))
        uarr = mesh.element_nodal_fields[field][nelem]
        for j in np.arange(n):
            for i in np.arange(n):
                k = j*n + i
                derivative_u_local[k,0] = np.sum(uarr * dll(dl, l, i, j))
                derivative_u_local[k,1] = np.sum(uarr * dll(l, dl, i, j))
        for i in range(n**2):
            result[nelem,i] = np.linalg.inv(J[nelem,i]).dot(derivative_u_local[i])
        mesh.attach_field('grad_x', result[:,:,0])
        mesh.attach_field('grad_y', result[:,:,1])
    return result


# + tags=[]
def rhs_init_square(p):

    x = p[:, :, 0]
    y = p[:, :, 1]

    rhs = np.zeros_like(x)
    delta = 0.05
    mask = (x > 0.2 - delta) * (x < 0.2 + delta) * (y > 0.35 - delta) * (y < 0.35 + delta)
    rhs[mask] = 150.
    delta = 0.1
    mask = (x > 0.7 - delta) * (x < 0.7 + delta) * (y > 0.2 - delta) * (y < 0.2 + delta)
    rhs[mask] = 120.
    
    mask = (x > 0.6 - delta) * (x < 0.6 + delta) * (y > 0.7 - delta) * (y < 0.7 + delta)
    rhs[mask] = 80.
    
    return rhs

def rhs_init_quad(p):

    x = p[:, :, 0]
    y = p[:, :, 1]

    rhs = (1 - np.sqrt((x-0.5)**2 + (y-0.5)**2))*39.3
    
    return rhs

def rhs_init_uniform(p, const = 1.):
    assert(const != 0)
    x = p[:, :, 0]
    rhs = np.ones_like(x)*const
    
    return rhs

def simulation(mesh,  output_filename = "simulation.h5",input_filename = "rhs_2d.h5"):
    w = sn.simple_config.simulation.Poisson(mesh=mesh)
    w.domain.polynomial_order = mesh.shape_order

    w.physics.poisson_equation.right_hand_side.filename = input_filename 
    w.physics.poisson_equation.right_hand_side.format = "hdf5"
    w.physics.poisson_equation.right_hand_side.field = "rhs"

    w.physics.poisson_equation.solution.filename = output_filename
    w.physics.poisson_equation.mass_matrix_scaling = MASS_SCALING

    boundaries = sn.simple_config.boundary.HomogeneousDirichlet(
        side_sets=["x0", "x1", "y0", "y1"]
    )

    # Associate boundaries with our simulation.
    w.add_boundary_conditions(boundaries)

    w.solver.max_iterations = 300
    w.solver.absolute_tolerance = 0.0
    w.solver.relative_tolerance = 1e-10
    w.solver.preconditioner = PRECONDITIONER
    w.validate()
    # -

    api.run(
        input_file=w,
        site_name="local",
        output_folder="2d_simulation",
        overwrite=True,
        ranks=NUM_RANKS,
        verbosity=VERBOSITY,
    )
    

def new_mesh(rhs, output_filename="rhs_2d.h5"):
    m = simple_mesh.CartesianHomogeneousIsotropicElastic2D(
    vp=2.0, vs=1.0, rho=1.0, x_max=1.0, y_max=1.0, max_frequency=10.0)

    m.advanced.tensor_order = TENSOR_ORDER
    mesh = m.create_mesh()

    f = np.ones_like(mesh.elemental_fields["VP"])

    points = mesh.points[mesh.connectivity]

    mesh.elemental_fields = {}
    mesh.attach_field("M0", 1. * f)
    mesh.attach_field("M1", M1 * f)

    mesh.elemental_fields["fluid"] = np.ones([mesh.nelem])
    
    if type(rhs) == str:
        if rhs == 'init':
            rhs = rhs_init_uniform(points, const = 10**-5)
    else:
        rhs = rhs
        
    if rhs.shape != f.shape:
        rhs = rhs[mesh.connectivity]
    mesh.attach_field('fluid', np.ones(mesh.nelem))
    mesh.attach_field("rhs", rhs)
    mesh.write_h5(output_filename)
    return mesh

def new_mesh_obs(rhs, output_filename="rhs_obs.h5"):
    m = simple_mesh.CartesianHomogeneousIsotropicElastic2D(
    vp=2.0, vs=1.0, rho=1.0, x_max=1.0, y_max=1.0, max_frequency=10.0)

    m.advanced.tensor_order = TENSOR_ORDER
    mesh = m.create_mesh()

    f = np.ones_like(mesh.elemental_fields["VP"])

    points = mesh.points[mesh.connectivity]

    mesh.elemental_fields = {}
    mesh.attach_field("M0", 1.0 * f)
    mesh.attach_field("M1", M1  * f)

    mesh.elemental_fields["fluid"] = np.ones([mesh.nelem])
    
    rhs = rhs_init_square(points)
  #  rhs = rhs_init_quad(points)
    if rhs.shape != f.shape:
        rhs = rhs[mesh.connectivity]
    mesh.attach_field('fluid', np.ones(mesh.nelem))
    mesh.attach_field("rhs", rhs)
    mesh.write_h5(output_filename)
    return mesh

# def steplength_v2(J, grad_p, M, d_obs, A, P, sigmacoef=1):
#     c = J
#     b = np.linalg.norm(grad_p)
    
#     output_filename="sigma_test"
#     sigmatest = J * sigmacoef 
    
#     rhs = np.array(M + sigmatest * grad_p, dtype=np.float32)
#     mesh_test = new_mesh(rhs, output_filename="rhs_2d_test.h5")
#     simulation(mesh_test, output_filename, input_filename="rhs_2d_test.h5")
#     Utest = UnstructuredMesh.from_h5('2d_simulation/' + output_filename).element_nodal_fields['solution']
    
#     Jtest = A.dot((d_obs - U).ravel()).T.dot(P).dot(A).dot((d_obs - U).ravel())
    
#     a = sigmatest**(-2) * (Jtest - b * sigmatest - c) 
     
#     return (-0.5) * b / a

def compute_model(M, input_filename = "rhs_2d.h5", output_filename = "simulation"):
    rhs = np.array(M, dtype=np.float32)
    mesh = new_mesh(rhs, output_filename=input_filename)
    simulation(mesh, output_filename=output_filename, input_filename=input_filename)
    return UnstructuredMesh.from_h5('2d_simulation/' + output_filename).element_nodal_fields['solution']

def compute_forward(M, input_filename = "rhs_2d.h5", output_filename = "simulation"):
    rhs = np.array(M, dtype=np.float32)
    mesh = new_mesh(rhs, output_filename=input_filename)
    simulation(mesh, output_filename=output_filename, input_filename=input_filename)
    return UnstructuredMesh.from_h5('2d_simulation/' + output_filename).element_nodal_fields['solution']

def compute_gradient(V):
    mesh_a = new_mesh(V, output_filename="rhs_2d_adj.h5")
    simulation(mesh_a, output_filename = "adjoint_state.h5", input_filename = "rhs_2d_adj.h5")
    adjoint = UnstructuredMesh.from_h5("2d_simulation/adjoint_state.h5").element_nodal_fields['solution']
    Z = np.zeros_like(mesh_a.points[:,0])
    Z[mesh_a.connectivity] = adjoint
    return -1./M1 * Z

def steplength_v2(J, grad_p, M, V, d_obs, A, P, coef):
    c = J
    b = np.linalg.norm(grad_p)

    sigmatest =np.max(np.abs(V))* coef#np.max(np.abs(V))*coef
    Utest = compute_model(M + sigmatest * (-1) * gradJ,  output_filename = "sigmatest")
    
    Jtest = misfit(d_obs, Utest, A, P)
    
    a = sigmatest**(-2) * (Jtest - b*sigmatest - c) 
    return (-0.5) * b / a

def wolfe_conditions(Mtest, J, grad_p, gradJ, sigma, c1=10**(-4), c2=0.1):
    Utest = compute_model(Mtest)
    Jtest = misfit(d_obs, Utest, A, P)
    V = d_obs - Utest
    adjoint = compute_model(V, input_filename = "adj_rhs_2d.h5", output_filename = "simulation_adjoint")
    Z = np.zeros_like(mesh_obs.points[:,0])
    gradJtest = np.zeros_like(mesh_obs.points[:,0])
    Z[mesh_obs.connectivity] = adjoint
    gradJtest = -1./ M1 * Z 
    
    armijo = Jtest <= (J + c1 * sigma * gradJ.T.dot(grad_p))
    curvature = np.abs(gradJtest.T.dot(grad_p)) <= (-1)*c2*gradJ.T.dot(grad_p)
    
    if armijo and curvature:
        return True
    else:
        print(f'armijo:{armijo}, curvature:{curvature}')
        return False
    
def armijo_condition(Mtest, J, gradJ, grad_p, sigma, c1=10**(-4), curvat = True):
    Utest = compute_model(Mtest)
    Jtest = misfit(d_obs, Utest, A, P)
    if curvat:
        print(f'curvature condition: {curvature_condition(Utest, gradJ, sigma)}')
    
    #if Jtest <= (J + c1 * sigma * np.linalg.norm(grad_p)):
    if Jtest <= (J + c1 * sigma * gradJ.T.dot(grad_p/np.linalg.norm(grad_p))):
        return True
    else:
        print(Jtest, J, c1 * sigma * gradJ.T.dot(grad_p/np.linalg.norm(grad_p)))
        return False
    
def curvature_condition(Utest, gradJ, sigma, c2=0.1):
    V = d_obs - Utest
    adjoint = compute_model(V, input_filename = "adj_rhs_2d.h5", output_filename = "simulation_adjoint")
    
    Z = np.zeros_like(mesh_obs.points[:,0])
    gradJtest = np.zeros_like(mesh_obs.points[:,0])
    
    Z[mesh_obs.connectivity] = adjoint

    gradJtest = -1./ M1 * Z 
    
    if gradJtest.T.dot(gradJ) >= c2*gradJ.T.dot((-1)*gradJ):
        return True
    else: return False
    
def test_steplength(M, sigma, J, h_prev):
    M += sigma * h_prev 
    mesh_f = new_mesh(M[mesh_f.connectivity])
    simulation(mesh_f, output_filename = "forward_simulation.h5", input_filename = "rhs_2d.h5")
    U = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5").element_nodal_fields['solution']
    Jtest = A.dot((d_obs - U).ravel()).T.dot(P).dot(A).dot((d_obs - U).ravel())
    if Jtest < J:
        return True
    else:
        return False
    
def dll(dl,l, nx, ny):
    #param: nx and ny are the coordinates of the nodal point in the local c.s.
    #return: 
    n = dl.shape[0]
    l_local = np.zeros(n**2)
    for j in range(n):
        for i in range(n):
            k = j*5 + i
            l_local[k] = dl[nx, i]*l[ny, j]
    return l_local


# -






# +
# mesh_f = new_mesh('init')
# simulation(mesh_f, output_filename = "forward_simulation.h5", input_filename = "rhs_2d.h5")

# mesh_i = mesh_f.copy()
# sol_i = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5")

#d_obs:
mesh_obs = new_mesh_obs('init')
simulation(mesh_obs, output_filename='obs.h5', input_filename='rhs_obs.h5')
d_obs = UnstructuredMesh.from_h5("2d_simulation/obs.h5").element_nodal_fields['solution']
mesh_obs.attach_field('gravpot', d_obs)
mesh_obs.write_h5('mesh_obs.h5')

# +
#nodal points array raveled
D = d_obs.ravel()
N_d_nodes = D.shape[0]

#Picking the meaningful observational points. A maps N_d_nodes -> N_d_obs
N_d_obs = N_d_nodes
A = np.zeros((N_d_obs, N_d_nodes))
if N_d_obs == N_d_nodes:
    A = np.identity(N_d_nodes) 

# for i in range(0,N_d_obs, 25):
#     A[i,i] = 1
P = np.identity(N_d_obs)

J_arr = []
gradJ_arr = []

#Inverse problem (conjugate gradient from Fichtner 2011):
#zeroth step:
M = np.ones(mesh_obs.points.shape[0])*10**-2
U = compute_forward(M)
#U = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5").element_nodal_fields['solution']
V = d_obs - U
print('V',np.mean(V))
J = misfit(d_obs, U, A, P) 
J_arr.append(J)
print('J', J,'rel J', J/np.sum(A.dot(d_obs.ravel())))

gradJ = compute_gradient(V)
h_prev = (-1)*gradJ
gradJ_norm_prev = np.linalg.norm(gradJ)
# -


gradtest = np.zeros_like(M)
for i in range(5,M.shape[0]-5):
    d = 0.01
    Mtest = M.copy()
    Mtest[i] += d
    Utest = compute_forward(Mtest)
    Jtest = misfit(d_obs, Utest, A, P)
    gradtest[i] = (J - Jtest)/d

gradtest/gradJ

mesh_obs.connectivity

mesh_obs.connectivity.shape

np.where(gradJ == np.min(gradJ))

# + tags=[]
niter=100
M = np.zeros(mesh_obs.points.shape[0])
for i in range(niter):
    J_arr.append(J)
    gradJ_arr.append(gradJ_norm_prev)
    #M[mesh_obs.connectivity] = mesh_.elemental_fields['rhs']
    
    grad_p = h_prev 

    coef = 1000.
    sigma = steplength_v2(J, grad_p, M, V, d_obs, A, P, coef=coef)
    while not armijo_condition(M+sigma * grad_p, J, gradJ, grad_p, sigma, c1=10**(-4), curvat = False):
        print('false armijo', coef)
        coef /= 2.
        sigma = steplength_v2(J, grad_p, M, V, d_obs, A, P, coef=coef)

    #update model:
    M += sigma * grad_p
    
    #new forward simulation

    U = compute_forward(M)
    
    #find new model gradient:
    
    J = misfit(d_obs, U, A, P)
    print(i, 'J', J,'rel J', J/np.sum(A.dot(d_obs.ravel())))
    
    V = d_obs - U
    
    gradJ = compute_gradient(V)
    gradJ_norm_new = np.linalg.norm(gradJ)
    
    #find a new direction:
    h_new = (-1) * gradJ + gradJ_norm_new**2 / gradJ_norm_prev**2 * h_prev
    gradJ_norm_prev = gradJ_norm_new
    h_prev = h_new



fig, [ax1,ax2] = plt.subplots(2,1,figsize=(10,10))
ax1.plot(J_arr)
ax1.set_xlabel('iteration')
ax1.set_ylabel('J')

ax2.plot(gradJ_arr)
ax2.set_xlabel('iteration')
ax2.set_ylabel('L2(grad)')





# sol = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5")
# final_U = sol.element_nodal_fields['solution']
# mesh_f.attach_field('difference', abs(mesh_f.elemental_fields['rhs'] - mesh_obs.elemental_fields['rhs'])/mesh_obs.elemental_fields['rhs'])
# mesh_f.attach_field('init_m', mesh_obs.elemental_fields['rhs'])
# mesh_f.write_h5('test.h5')

#   new forward simulation
#     mesh_f = new_mesh(M[mesh_f.connectivity])
#     simulation(mesh_f, output_filename = "forward_simulation.h5", input_filename = "rhs_2d.h5")
#     U = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5").element_nodal_fields['solution']
# -











A

# +
mesh_f = new_mesh('init')
simulation(mesh_f, output_filename = "forward_simulation.h5", input_filename = "rhs_2d.h5")

mesh_i = mesh_f.copy()
sol_i = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5")

#d_obs:
mesh_obs = new_mesh_obs('init')
simulation(mesh_f, output_filename='obs.h5', input_filename='rhs_obs.h5')
d_obs = UnstructuredMesh.from_h5("2d_simulation/obs.h5").element_nodal_fields['solution']
mesh_obs.attach_field('gravpot', d_obs)
mesh_obs.write_h5('mesh_obs.h5')

#nodal points array raveled
D = d_obs.ravel()
N_d_nodes = D.shape[0]

#Picking the meaningful observational points. A maps N_d_nodes -> N_d_obs
N_d_obs = N_d_nodes
A = np.zeros((N_d_obs, N_d_nodes))
# if N_d_obs == N_d_nodes:
#     A = np.identity(N_d_nodes) 

for i in range(0, N_d_obs, 25):
    A[i,i] = 1
P = np.identity(N_d_obs)

J_arr = []
gradJ_arr = []

#Inverse problem (conjugate gradient from Fichtner 2011):
#zeroth step:
U = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5").element_nodal_fields['solution']
V = d_obs - U
print('V',np.mean(V))
J = misfit(d_obs, U, A, P) 
J_arr.append(J)
print('J', J,'rel J', J/np.sum(A.dot(d_obs.ravel())))

mesh_a = new_mesh(V, output_filename="rhs_2d_adj.h5")
simulation(mesh_a, output_filename = "adjoint_state.h5", input_filename = "rhs_2d_adj.h5")
adjoint = UnstructuredMesh.from_h5("2d_simulation/adjoint_state.h5").element_nodal_fields['solution']

Z = np.zeros_like(mesh_a.points[:,0])
M = np.zeros_like(mesh_a.points[:,0])
Z[mesh_a.connectivity] = adjoint

gradJ = -1./M1 * Z
h_prev = (-1)*gradJ
gradJ_norm_prev = np.linalg.norm(gradJ)

niter=100
for i in range(niter):
    J_arr.append(J)
    gradJ_arr.append(gradJ_norm_prev)
    #find the optimal steplength sigma:
    M[mesh_a.connectivity] = mesh_f.elemental_fields['rhs']
    M[M<0] = 0
    
    grad_p = h_prev 

    coef = 1000.
    sigma = steplength_v2(J, grad_p, M, V, d_obs, A, P, coef=coef)
    while not armijo_condition(M+sigma * grad_p, J, gradJ, grad_p, sigma, c1=10**(-4), curvat = False):
        print('false armijo', coef, gradJ.T.dot(grad_p))
        coef *= 0.5
        sigma = steplength_v2(J, grad_p, M, V, d_obs, A, P, coef=coef)
#    print('armijo', coef, armijo_condition(M+sigma * grad_p, J, gradJ, grad_p, sigma, c1=10**(-4), curvat = False))
    #update model:
    M += sigma * grad_p
    
  #  print('gradJ:',np.mean(gradJ), sigma, np.mean(M))
    #new forward simulation
    mesh_f = new_mesh(M[mesh_f.connectivity])
    simulation(mesh_f, output_filename = "forward_simulation.h5", input_filename = "rhs_2d.h5")
    
    #find new model gradient:
    U = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5").element_nodal_fields['solution']
    J = misfit(d_obs, U, A, P)
    print(i, 'J', J,'rel J', J/np.sum(A.dot(d_obs.ravel())))
#     THRESHOLD = 0.01
#     if J/J_arr[0] < THRESHOLD:
#         break
    V = d_obs - U
    
    mesh_a = new_mesh(V, output_filename="rhs_2d_adj.h5")
    simulation(mesh_a, output_filename = "adjoint_state.h5", input_filename = "rhs_2d_adj.h5")
    adjoint = UnstructuredMesh.from_h5("2d_simulation/adjoint_state.h5").element_nodal_fields['solution']
    
    Z = np.zeros_like(mesh_a.points[:,0])
    Z[mesh_a.connectivity] = adjoint
    
    gradJ = -1./M1 * Z
    gradJ_norm_new = np.linalg.norm(gradJ)
    
    #find a new direction:
    h_new = (-1) * gradJ + gradJ_norm_new**2 / gradJ_norm_prev**2 * h_prev
    gradJ_norm_prev = gradJ_norm_new
    h_prev = h_new

# sol = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5")
# final_U = sol.element_nodal_fields['solution']
# mesh_f.attach_field('difference', abs(mesh_f.elemental_fields['rhs'] - mesh_obs.elemental_fields['rhs'])/mesh_obs.elemental_fields['rhs'])
# mesh_f.attach_field('init_m', mesh_obs.elemental_fields['rhs'])
# mesh_f.write_h5('test.h5')

fig, [ax1,ax2] = plt.subplots(2,1,figsize=(10,10))
ax1.plot(J_arr)
ax1.set_xlabel('iteration')
ax1.set_ylabel('J')

ax2.plot(gradJ_arr)
ax2.set_xlabel('iteration')
ax2.set_ylabel('L2(grad)')

# +
mesh_f = new_mesh('init')
simulation(mesh_f, output_filename = "forward_simulation.h5", input_filename = "rhs_2d.h5")

mesh_i = mesh_f.copy()
sol_i = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5")

#d_obs:
mesh_obs = new_mesh_obs('init')
simulation(mesh_f, output_filename='obs.h5', input_filename='rhs_obs.h5')
d_obs = UnstructuredMesh.from_h5("2d_simulation/obs.h5").element_nodal_fields['solution']
mesh_obs.attach_field('gravpot', d_obs)
mesh_obs.write_h5('mesh_obs.h5')

#nodal points array raveled
D = d_obs.ravel()
N_d_nodes = D.shape[0]

#Picking the meaningful observational points. A maps N_d_nodes -> N_d_obs
N_d_obs = N_d_nodes
A = np.zeros((N_d_obs, N_d_nodes))
# if N_d_obs == N_d_nodes:
#     A = np.identity(N_d_nodes) 


A[35,35] = 1
P = np.identity(N_d_obs)

J_arr = []
gradJ_arr = []

#Inverse problem (conjugate gradient from Fichtner 2011):
#zeroth step:
U = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5").element_nodal_fields['solution']
V = d_obs - U
print('V',np.mean(V))
J = misfit(d_obs, U, A, P) 
J_arr.append(J)
print('J', J,'rel J', J/np.sum(A.dot(d_obs.ravel())))

mesh_a = new_mesh(V, output_filename="rhs_2d_adj.h5")
simulation(mesh_a, output_filename = "adjoint_state.h5", input_filename = "rhs_2d_adj.h5")
adjoint = UnstructuredMesh.from_h5("2d_simulation/adjoint_state.h5").element_nodal_fields['solution']

Z = np.zeros_like(mesh_a.points[:,0])
M = np.zeros_like(mesh_a.points[:,0])
Z[mesh_a.connectivity] = adjoint

gradJ = -1./M1 * Z
h_prev = (-1)*gradJ
gradJ_norm_prev = np.linalg.norm(gradJ)

niter=100
for i in range(niter):
    J_arr.append(J)
    gradJ_arr.append(gradJ_norm_prev)
    #find the optimal steplength sigma:
    M[mesh_a.connectivity] = mesh_f.elemental_fields['rhs']
    
    grad_p = h_prev 

    coef = 1000.
    sigma = steplength_v2(J, grad_p, M, V, d_obs, A, P, coef=coef)
    while not armijo_condition(M+sigma * grad_p, J, gradJ, grad_p, sigma, c1=10**(-4), curvat = False):
        print('false armijo', coef, gradJ.T.dot(grad_p))
        coef *= 0.5
        sigma = steplength_v2(J, grad_p, M, V, d_obs, A, P, coef=coef)
#    print('armijo', coef, armijo_condition(M+sigma * grad_p, J, gradJ, grad_p, sigma, c1=10**(-4), curvat = False))
    #update model:
    M += sigma * grad_p
    
  #  print('gradJ:',np.mean(gradJ), sigma, np.mean(M))
    #new forward simulation
    mesh_f = new_mesh(M[mesh_f.connectivity])
    simulation(mesh_f, output_filename = "forward_simulation.h5", input_filename = "rhs_2d.h5")
    
    #find new model gradient:
    U = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5").element_nodal_fields['solution']
    J = misfit(d_obs, U, A, P)
    print(i, 'J', J,'rel J', J/np.sum(A.dot(d_obs.ravel())))
#     THRESHOLD = 0.01
#     if J/J_arr[0] < THRESHOLD:
#         break
    V = d_obs - U
    
    mesh_a = new_mesh(V, output_filename="rhs_2d_adj.h5")
    simulation(mesh_a, output_filename = "adjoint_state.h5", input_filename = "rhs_2d_adj.h5")
    adjoint = UnstructuredMesh.from_h5("2d_simulation/adjoint_state.h5").element_nodal_fields['solution']
    
    Z = np.zeros_like(mesh_a.points[:,0])
    M = np.zeros_like(mesh_a.points[:,0])
    Z[mesh_a.connectivity] = adjoint
    
    gradJ = -1./M1 * Z
    gradJ_norm_new = np.linalg.norm(gradJ)
    
    #find a new direction:
    h_new = (-1) * gradJ + gradJ_norm_new**2 / gradJ_norm_prev**2 * h_prev
    gradJ_norm_prev = gradJ_norm_new
    h_prev = h_new

# sol = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5")
# final_U = sol.element_nodal_fields['solution']
# mesh_f.attach_field('difference', abs(mesh_f.elemental_fields['rhs'] - mesh_obs.elemental_fields['rhs'])/mesh_obs.elemental_fields['rhs'])
# mesh_f.attach_field('init_m', mesh_obs.elemental_fields['rhs'])
# mesh_f.write_h5('test.h5')

fig, [ax1,ax2] = plt.subplots(2,1,figsize=(10,10))
ax1.plot(J_arr)
ax1.set_xlabel('iteration')
ax1.set_ylabel('J')

ax2.plot(gradJ_arr)
ax2.set_xlabel('iteration')
ax2.set_ylabel('L2(grad)')
# -















grad_bf = np.zeros(M.shape)
grad_bf_j3 = np.zeros(M.shape)
dM = 0.05
M[mesh_a.connectivity] = mesh_f.elemental_fields['rhs']
J = A.dot((d_obs - U).ravel()).T.dot(P).dot(A).dot((d_obs - U).ravel())
for i in range(M.shape[0]):
    M2 = M.copy()
    M2[i] += dM
    mesh_f = new_mesh(M2[mesh_f.connectivity])
    simulation(mesh_f, output_filename = "forward_simulation.h5", input_filename = "rhs_2d.h5")
    U2 = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5").element_nodal_fields['solution']
    J2 = A.dot((d_obs - U2).ravel()).T.dot(P).dot(A).dot((d_obs - U2).ravel())
    J3 = np.linalg.norm((d_obs - U2).ravel())
    grad_bf[i] = (J2 - J)/dM
    grad_bf_j3[i] = (J3 - J)/dM

grad_bf/np.linalg.norm(grad_bf)

(grad_bf/np.linalg.norm(grad_bf)) / (gradJ/np.linalg.norm(gradJ))



















# +
for i in range(niter):
    gradJ_arr.append(gradJ_norm_prev)
    #find the optimal steplength sigma:
    M[mesh_a.connectivity] = mesh_f.elemental_fields['rhs']
    #sigma = steplength(V, h_prev, M, d_obs)
    sigma = steplength_v2(J, gradJ, M, V, d_obs, A, P)
    #update model:
    M += sigma * h_prev
  #  print('gradJ:',np.mean(gradJ), sigma, np.mean(M))
    #mesh_f.elemental_fields['rhs'] += (sigma * h_prev)[mesh_f.connectivity]
    
    #new forward simulation
    mesh_f = new_mesh(M[mesh_f.connectivity])
    simulation(mesh_f, output_filename = "forward_simulation.h5", input_filename = "rhs_2d.h5")
    
    #find new model gradient:
    U = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5").element_nodal_fields['solution']
    V = d_obs - U
    J = A.dot((d_obs - U).ravel()).T.dot(P).dot(A).dot((d_obs - U).ravel())
    J_arr.append(J)
    print(i, 'J', J,'rel J', J/np.sum(A.dot(d_obs.ravel())))
    THRESHOLD = 0.01
    if J/J_arr[0] < THRESHOLD:
        break
    
    mesh_a = new_mesh(V, output_filename="rhs_2d_adj.h5")
    simulation(mesh_a, output_filename = "adjoint_state.h5", input_filename = "rhs_2d_adj.h5")
    adjoint = UnstructuredMesh.from_h5("2d_simulation/adjoint_state.h5").element_nodal_fields['solution']
    
    Z = np.zeros_like(mesh_a.points[:,0])
    M = np.zeros_like(mesh_a.points[:,0])
    Z[mesh_a.connectivity] = adjoint
    
    gradJ = -1./M1 * Z
    gradJ_norm_new = np.linalg.norm(gradJ)
    
    #find a new direction:
    h_new = (-1) * gradJ + gradJ_norm_prev**2 / gradJ_norm_new**2 * h_prev
    gradJ_norm_prev = gradJ_norm_new
    h_prev = h_new
    
sol = UnstructuredMesh.from_h5("2d_simulation/forward_simulation.h5")
final_U = sol.element_nodal_fields['solution']
mesh_f.attach_field('difference', abs(mesh_f.elemental_fields['rhs'] - mesh_obs.elemental_fields['rhs'])/mesh_obs.elemental_fields['rhs'])
mesh_f.attach_field('init_m', mesh_obs.elemental_fields['rhs'])
mesh_f.write_h5('test.h5')

# fig, [ax1,ax2] = plt.subplots(2,1,figsize=(10,10))
# ax1.plot(J_arr)
# ax1.set_xlabel('iteration')
# ax1.set_ylabel('J')

# ax2.plot(grad_arr)
# ax2.set_xlabel('iteration')
# ax2.set_ylabel('L1(grad)')

print('finished')

# +
c = J
b = np.linalg.norm(gradPsy)

output_filename="sigma_test"
sigmatest = np.max(np.abs(V))*10#10**2 #10**30

rhs = np.array(M + sigmatest * gradJ, dtype=np.float32)
mesh_test = new_mesh(rhs)
forward_simulation(mesh_test, output_filename)
Utest = UnstructuredMesh.from_h5('2d_simulation/' + output_filename).element_nodal_fields['solution']

Jtest = A.dot((d_obs - U).ravel()).T.dot(P).dot(A).dot((d_obs - U).ravel())

a = sigmatest**(-2) * (Jtest - b - c) 

return (-0.5) * b / a
# -

sigmatest

V


